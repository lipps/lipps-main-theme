<?php

/**
 * Template Name: Salon archive
 *
 * @package LIPPS
 */

?>

<?php get_header(); ?>
<main>
  <div class="lipps-container">
    <div class="lipps-content-heading-ruby">店舗紹介</div>
    <h2 class="lipps-content-heading">SALON</h2>
    <?php
      if ( function_exists( 'lipps_salon_list' ) ) {
          echo lipps_salon_list();
      }
      ?>
  </div>
</main>
<?php get_footer(); ?>
