<?php
/**
 * The template for 404 page
 *
 * @package LIPPS Product
 */
?>

<?php get_header(); ?>
<main>
	<?php
	  $post_id = (get_page_by_path('page-not-found' ))->ID;
	  $local_post = get_post($post_id);
    $content = apply_filters('the_content', $local_post->post_content);
    $content = str_replace(']]>', ']]&gt', $content);
	?>
  <article id="post-<?php echo $post_id ?>">
      <?php echo $content; ?>
  </article>
  <?php get_template_part( 'template-parts/product', 'menu' ) ?>
</main>

<?php get_footer(); ?>
