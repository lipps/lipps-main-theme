<?php
/**
 * Template Name: Staff archive
 *
 * @package LIPPS
 */
?>

<?php
// 後々サロンが追加されたときにもタブ切り替えが正しく動作するように
// CSSをこの場で追加する
function lipps_staff_style_changer() {
	$salons = get_posts( array(
		'post_status' => 'publish',
		'post_type' => 'salon',
		'orderby' => 'date',
		'order' => 'ASC',
		'posts_per_page' => -1
	));

	$selector1 = $selector2 = '';

	foreach( $salons as $salon ) {
    $salon_id = '#staff-salon-'.$salon->post_name;
    $selector1 .= $salon_id.':checked ~ '.$salon_id.', ';
	  $selector2 .= $salon_id.':checked + .staff-salon-tab-item, ';
  }

  $selector1 = rtrim($selector1, ', ');
  $selector2 = rtrim($selector2, ', ');

  echo '<style>'.$selector1.' { display: block; } '.$selector2.' {color: #e61517; } </style>';
}
add_action( 'wp_head', 'lipps_staff_style_changer');
?>

<?php get_header();
// カスタムフィールドの値でループを回すために一覧を取得
// 毎回acf-fieldを全部とってきてループを回すのは無駄だが、
// 今回はshifterを使うため処理時間は考えなくてよいので、この方法を用いる
$acf_posts = get_posts( array(
    'post_type' => 'acf-field',
    'posts_per_page' => -1
    ) );

$category = '';
foreach ( $acf_posts as $acf_post ) {
	if ( $acf_post->post_excerpt !== 'category' ) continue;
	if ( get_post($acf_post->post_parent)->post_title  !== 'スタッフ' ) continue;

  $category = unserialize( $acf_post->post_content )['choices'];
}
?>

<main>
  <script async src="<?php echo get_template_directory_uri(); ?>/js/staff-tab-checker.js" charset="utf-8"></script>
  <div class="staff-tab-container">
    <?php
    $salons = get_posts( array(
        'post_status' => 'publish',
        'post_type' => 'salon',
        'orderby' => 'data',
        'order' => 'ASC',
        'posts_per_page' => -1
    ));
    foreach( $salons as $salon ) :
	    $custom_field_salon = get_post_meta( $salon->ID );
		$roman_name = $custom_field_salon['romanization'][0];
		$salon_name = $salon->post_title;
		$checked = $roman_name==='HARAJUKU' ? 'checked' : '';?>

      <input id="staff-salon-<?php echo $salon->post_name?>" type="radio" name="staff-tab" <?php echo $checked; ?>>
      <label class="staff-salon-tab-item" for="staff-salon-<?php echo $salon->post_name?>"><?php echo $salon_name; ?></label>
    <?php endforeach; ?>

	  <?php
	  foreach( $salons as $salon ) :
		  $custom_field_salon = get_post_meta( $salon->ID ); ?>

        <div class="lipps-container tab-content" id="staff-salon-<?php echo $salon->post_name; ?>">
          <h2 class="lipps-content-heading no-uppercase"><?php echo $custom_field_salon['romanization'][0]?></h2>
		    <?php
		    foreach( $category as $key => $cat ) :
			    $staffs = get_posts( array(
				    'post_status' => 'publish',
				    'post_type' => 'staff',
				    'posts_per_page' => -1,
				    'orderby' => 'date',
				    'order' => 'ASC',
				    'meta_query' => array(
					    'relation' => 'AND',
					    array(
						    'key' => 'salon',
						    'value' => $salon->ID,
						    'compare' => 'LIKE'
					    ),
					    array(
						    'key' => 'category',
						    'value' => $key,
						    'compare' => '='
					    )
				    )
			    ) ); ?>

			    <?php if ( !empty( $staffs ) ) { echo'<div class="staff-category">'.$cat.'</div>'; } ?>
              <div class="lipps-archive-box staff">
				  <?php
				  foreach( $staffs as $staff ) :
					  $custom_field_staff = get_post_meta( $staff->ID );
					  $staff_link = get_the_permalink( $staff->ID );
					  $staff_name = $staff->post_title;
					  $staff_position = $custom_field_staff['position'][0];
					  $staff_position = mb_convert_kana($staff_position, 'KVrn');
					  $staff_image = wp_get_attachment_image_src( $custom_field_staff['photo_main'][0], 'medium', false ); ?>

                    <div class="lipps-image-item staff-archive">
                      <a href="<?php echo $staff_link ?>"  class="lipps-box-link"></a>
                      <img src="<?php echo esc_url( $staff_image[0] ); ?>" alt="" class="staff-image">
                      <p class="staff-name"><?php echo $staff_name; ?></p>
                      <p class="staff-position"><?php echo $staff_position; ?></p>
                    </div>

				  <?php endforeach; ?>
              </div>
		    <?php endforeach; ?>
        </div>
	  <?php endforeach; ?>
  </div>
</main>

<?php get_footer(); ?>
