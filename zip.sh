#!/bin/sh

RANDOM=$(openssl rand -base64 12 | fold -w 10 | head -1)
BUILDS=`echo ${RANDOM} | sed -e "s/\///"`
sed -i -e "s/BUILDS/$BUILDS/g" style.css

zip -r "$BUILDS.zip" * -x "*.zip" ".*"

sed -i -e "s/$BUILDS/BUILDS/g" style.css
