<?php
/**
 * functions and definitions for LIPPS theme
 *
 * @link
 *
 * @package WordPress
 * @subpackage LIPPS
 * @since 0.1
 */

function lipps_setup() {

  $GLOBALS['content_width'] = 1024;

  add_theme_support( 'automatic-feed-links' );
  add_theme_support( 'title-tag' );
  add_theme_support( 'post-thumbnails' );
  add_theme_support( 'align-wide' );

  add_theme_support('custom-logo',
      array(
          'flex-width' => true,
          'flex-height' => true,
      )
  );

  register_nav_menus(
      array(
          'main-menu'=> 'メインメニュー',
          'menu-media'=> 'メディアリンク',
          'menu-related-site'=> '関係サイトリンク',
          'menu-info'=> '会社情報リンク',
      )
  );

	//add_post_type_support('salon', 'page-attributes' );
	//add_post_type_support('staff', 'page-attributes' );
}
add_action( 'after_setup_theme', 'lipps_setup' );

function lipps_scripts() {
  // include normalize css
  wp_enqueue_style( 'normalize', get_theme_file_uri('/css/normalize.css') );

  // サイト用CSSの読み込み
  wp_enqueue_style( 'lipps-style_webflow', get_theme_file_uri('/css/webflow.css') );
//	wp_enqueue_style( 'lipps-style', get_theme_file_uri('/css/lipps-products.webflow.css') );
  wp_enqueue_style( 'lipps-style', get_theme_file_uri('/css/app.css') );

  // jQueryの読み込み
  wp_deregister_script( 'jquery' ); // デフォルトで読み込まれるjQueryの登録を抹消
  wp_enqueue_script( 'jquery', get_template_directory_uri().'/js/jquery-3.4.1.min.js', array(), '3.4.1', true );

  wp_enqueue_script( 'webflow', get_template_directory_uri().'/js/webflow.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'lipps_scripts' );


// 管理画面から投稿メニューを非表示
function lipps_remove_menus(){
	remove_menu_page( 'edit.php' );
}
add_action( 'admin_menu', 'lipps_remove_menus' );

// ショートコード
require get_template_directory() . '/inc/shortcode.php';

// カスタム関数の読み込み
require get_template_directory() . '/inc/pagination.php';

// ヘアスタイルのタームを自動付加
require get_template_directory() . '/inc/auto_set_terms.php';

// 便利関数群
require get_template_directory() . '/inc/utils.php';
