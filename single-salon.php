<?php
/**
 * The template for displaying page for salon
 *
 * @package LIPPS
 */
?>

<?php get_header(); ?>
<main>
  <?php while( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <?php
      the_content();
      ?>
    </article>
	<?php endwhile; ?>
</main>

<?php get_footer(); ?>
