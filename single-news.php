<?php
/**
 * The template for displaying news posts
 *
 * @package LIPPS
 */
?>

<?php get_header(); ?>
<main>
	<?php while ( have_posts() ) : the_post(); ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class('lipps-container'); ?>>
        <h2 class="lipps-content-heading">NEWS</h2>
        <h4 class="news-title"><?php the_title(); ?></h4>
        <div class="news-content">
          <?php
          the_content();
		      ?>
        </div>
      </article>
      <div class="news-return-link-container">
        <a href="<?php echo get_post_type_archive_link( 'news'  ); ?>" class="news-return-link left">< ARCHIVE</a>
      </div>
	<?php endwhile; ?>
</main>

<?php get_footer(); ?>
