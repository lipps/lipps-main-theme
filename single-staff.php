<?php
/**
 * The template for displaying pages for staff
 *
 * @package LIPPS
 */
?>

<?php get_header(); ?>

<main>
  <?php while( have_posts() ) : the_post(); ?>
  <article id="post-<?php the_ID(); ?>" <?php post_class('lipps-container' );?>>
    <?php
    $post_id = get_the_ID();

    $staff = get_post( $post_id );
    $custom_field = get_post_meta($post_id);

    $image = wp_get_attachment_image_src( $custom_field['photo_main'][0], 'large', false );

    $staff_intro = $custom_field['introduction'][0];
    $staff_intro = apply_filters( 'the_content', $staff_intro );
    $staff_intro = str_replace( ']]>', ']]&gt;', $staff_intro );

    $salon_id = unserialize($custom_field['salon'][0])[0];
    $salon_name = get_post($salon_id)->post_title;
    $salon_link = get_the_permalink( $salon_id );
    $salon_acf = get_post_meta($salon_id);
    $salon_phone_number = $salon_acf['phone_number'][0];

    $staff_position = $custom_field['position'][0];
    $staff_position = mb_convert_kana($staff_position, 'KVrn');
    $staff_position = $staff_position . ' / ';

    $staff_info = $custom_field['birthday'][0] . ' / ' . $custom_field['blood_type'][0] . ' / ' . $custom_field['hometown'][0];

    $image_monthly = '';
    if ( !empty( $custom_field['photo_monthly'][0] ) ) {
      $image_monthly = wp_get_attachment_image_src( $custom_field['photo_monthly'][0], 'large', false );
    }

    $monthly_comment = $custom_field['monthly_comment'][0];
    $monthly_comment = apply_filters( 'the_content', $monthly_comment );
    $monthly_comment = str_replace( ']]>', ']]&gt;', $monthly_comment );

    $staff_twitter = '';
    if ( !empty( $custom_field['twitter_account']) && !empty($custom_field['twitter_account'][0]) ) {
	    $staff_twitter = $custom_field['twitter_account'][0];
    }

    $staff_instagram = '';
    if ( !empty( $custom_field['instagram_account']) && !empty($custom_field['instagram_account'][0]) ) {
      $staff_instagram = $custom_field['instagram_account'][0];
    }

    $review_data = '';
    if ( !empty( $custom_field['review_url'][0] ) ) {
      $review_data = explode('/', $custom_field['review_url'][0]);
    }
    ?>

    <h2 class="lipps-content-heading">STAFF</h2>
    <div class="staff-container">
      <div class="staff-main-imagebox narrow">
        <img src="<?php echo esc_url( $image[0] ); ?>" alt="" class="staff-main-image">
        <div class="staff-text-container narrow">
          <div class="staff-name-detail"><?php the_title();?></div>
          <div class="staff-position-detail">
            <?php echo $staff_position;?>
            <a href="<?php echo $salon_link; ?>" class="staff-position-detail-link"><?php echo $salon_name ?></a>
          </div>
          <div class="staff-personal-info"><?php echo $staff_info;?> </div>
          <div class="staff-hobby">趣味: <?php echo $custom_field['hobby'][0];?> </div>
        </div>
      </div>
      <div class="staff-main-imagebox">
        <img src="<?php echo esc_url( $image[0] ); ?>" alt="" class="staff-main-image wide">
        <div class="staff-main-image-underbox">
          <div class="staff-calendar">
          <?php if ( (int)$custom_field['category'][0] === 1 ) : ?>
	          <?php echo do_blocks('<!-- wp:lipps/lipps-business-day-block {"post_id":'.$post_id.'} /-->'); ?>
          <?php endif; ?>
          </div>
          <div class="reservation-container">
            <?php if ( !empty($custom_field['reservation_url'][0]) ) : ?>
              <div class="reservation-item reservation">
                <a href="<?php echo esc_url($custom_field['reservation_url'][0]); ?>" class="lipps-box-link"></a>
                <p class="reservation-sentence">このスタイリストのWeb予約</p>
              </div>
            <?php endif; ?>
            <div class="reservation-item phone-call">
              <a href="tel:<?php echo str_replace('-', '', $salon_phone_number); ?>" class="lipps-box-link"></a>
              <p class="reservation-sentence"><?php echo $salon_phone_number; ?> 電話予約</p>
            </div>
            <div class="reservation-item coupon">
              <a href="<?php echo esc_url(home_url('coupon')); ?>" class="lipps-box-link"></a>
              <p class="reservation-sentence">クーポンはこちら</p>
            </div>
            <?php if ( !empty($custom_field['review_url'][0]) ) : ?>
              <div class="reservation-item review">
              <p id="open-review" class="reservation-sentence">口コミを見る</p>
            </div>
            <?php endif; ?>
          </div>
          <div class="staff-sns-container">
            <?php if ( !empty($staff_twitter) ) : ?>
            <div class="staff-sns twitter">
              <a href="<?php echo esc_url('https://twitter.com/'.$custom_field['twitter_account'][0]); ?>" target="_blank" class="lipps-box-link"></a>
              <svg aria-hidden="true" data-icon="twitter" class="staff-twitter-svg" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" value="fab-twitter">
                <path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path>
              </svg>
              <span class="staff-sns-title twitter">Twitter</span>
            </div>
            <?php endif; ?>
            <?php if ( !empty($staff_instagram) ) :?>
            <div class="staff-sns instagram">
              <a href="<?php echo esc_url('https://instagram.com/'.$staff_instagram); ?>" target="_blank" class="lipps-box-link"></a>
              <svg aria-hidden="true" data-icon="instagram" class="staff-instagram-svg" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" value="fab-instagram">
                <path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path>
              </svg>
              <span class="staff-sns-title instagram">Instagram</span>
            </div>
            <?php endif; ?>
          </div>
	        <?php
	        if ( !empty($staff_twitter) ) : ?>
		        <?php echo do_shortcode('[twitter-timeline '. $staff_twitter . ']'); ?>
	        <?php endif; ?>
        </div>
      </div>
      <div class="staff-text-container">
        <div class="staff-name-detail wide"><?php the_title();?></div>
        <div class="staff-position-detail wide">
	        <?php echo $staff_position;?>
          <a href="<?php echo $salon_link; ?>" class="staff-position-detail-link"><?php echo $salon_name ?></a>
        </div>
        <div class="staff-personal-info wide"><?php echo $staff_info;?> </div>
        <div class="staff-hobby wide">趣味: <?php echo $custom_field['hobby'][0];?> </div>
        <div class="staff-intro"><?php echo $staff_intro; ?> </div>
        <div class="monthly-message-heading">MONTHLY MESSAGE</div>
        <div class="monthly-message-box">
          <?php
          if ( !empty( $image_monthly ) ) : ?>
            <img src="<?php echo $image_monthly[0]; ?>" alt="PhotoMonthly" class="monthly-image">
          <?php endif; ?>
          <div class="monthly-comment"><?php echo $monthly_comment; ?></div>
        </div>
      </div>
    </div>
	  <?php
    if ( (int)$custom_field['category'][0] === 1 ) : ?>
        <div class="recent-hairstyle-title">スタイリスト <?php the_title(); ?>の新着</div>
        <div class="recent-hairstyle-subtitle">HAIR STYLE</div>
		  <?php echo do_shortcode('[recent-hairstyle-list '.$post_id . ']'); ?>
	  <?php endif; ?>
  </article>
  <?php endwhile; ?>
  <div id="staff-review" class="review-windows">
    <div id="close-review" class="review-background"></div>
    <div class="review-container">
      <img id = "review-loading" src="<?php echo get_template_directory_uri();?>/images/loading.gif" class="review-loading">
      <iframe id="review-iframe" src="" class="review-iframe" frameborder="no" data-id1="<?php echo $review_data[5]; ?>" data-id2="<?php echo $review_data[6];?>"></iframe>
    </div>
  </div>
</main>
<?php get_footer(); ?>
<script async src="<?php echo get_template_directory_uri(); ?>/js/review-popup.js" charset="utf-8"></script>
<?php
//          print_r(WP_Block_Type_Registry::get_instance()->get_registered( $t[0]['blockName'] )->render($t[0]['attrs'], '') );
//          print_r(render_block($t[0]));
//          $html = apply_filters( 'the_content', $html );
//          $html = str_replace( ']]>', ']]&gt;', $html );
?>


