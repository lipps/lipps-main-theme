
(function() {
    const url = ' https://script.google.com/macros/s/AKfycbxb97bcjrUnmXUfM-tK56zJ30Cm16ynZC05b2_J1w/exec';

    const json = new XMLHttpRequest();
    json.onreadystatechange = function() {
        if ((json.readyState === 4) && (json.status === 200)) {
            const data = JSON.parse(json.responseText);

            for (let ii = 0; ii < 2; ++ii) {
                const ch = 'https://www.youtube.com/embed/'+data[ii].videoId;
                const html = '<iframe class="youtube-item" class= "youtube-wedget" src="'+ch+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $('#lipps-youtube-recentposts').append(html);
            }
        }
    }
    json.open("GET",url, true);
    json.send(null);
})();