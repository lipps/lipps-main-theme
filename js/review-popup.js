// ポップアップを表示する関数
document.getElementById('open-review').onclick = () => {
    const parent = document.getElementById('staff-review');
    parent.style.display = 'block';

    const child1 = document.getElementById('review-loading');
    const child2 = document.getElementById('review-iframe');

    const w = window.getComputedStyle(child2, null).getPropertyValue('width').slice(0,-2);
    const h = window.getComputedStyle(child2, null).getPropertyValue('height').slice(0,-2);

    const property = '?main_backgroud_color=f3f3f3&main_border_color=0000ff&main_font_color=000000&btn_read_user_color1=3d85c6&btn_read_user_color2=3d85c6&btn_read_user_font_color=FFFFFF&btn_read_staff_color1=1171FF&btn_read_staff_color2=004FC6&btn_read_staff_font_color=FFFFFF&btn_back_color1=CCCCCC&btn_back_color2=999999&btn_back_font_color=FFFFFF&btn_reservation_color1=0000ff&btn_reservation_color2=0000ff&btn_reservation_font_color=FFFFFF&header_font_color=ffffff&header_background_color1=0000ff&header_background_color2=0000ff&user_box_background_color=f3f3f3&user_box_border_color=0000ff&staff_box_background_color=D7E7FF&staff_box_border_color=0064FF&footer_background_color=FFFFFF&header=1';
    const size = '&width='+w+'&height='+h;
    const url = 'https://cs.appnt.me/review_blog_parts/index01/'+child2.dataset.id1+'/'+child2.dataset.id2+'/'+property+size;

    if (child2.src != url) {
        child2.src = url;
        child2.onload = () => {
            child1.style.display = 'none';
            child2.style.display = 'block';
        }
    }
}

// ポップアップを隠す関数
document.getElementById('close-review').onclick = () => {
    var x = document.getElementById('staff-review');
    x.style.display = 'none';
}

