//const url = "https://script.googleusercontent.com/macros/echo?user_content_key=yOFMj0jCJDVJNiRDztta4PfP95Cz7vb7hagaYDrLA7tuh_I5-Lre4ivQneiISt6mNcPtNgr-KpIJ5SLKtRf3htIByP3qf-_vm5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnKNFwFAuQvvdnoh6_2J1ZIioTl8a1liLmjmH0KJ_bqgTZEp23OTa9J2HThtf971g1DOmbnRGq-tk&lib=MHVFd6a9INl2sTDOgF0YGQpFQUS8fmLSN"

(function() {
    const url = "https://script.google.com/macros/s/AKfycbyG7gynpWMVXZwk6l36QW5pVOeiuSA2lLSw_8sbK0HSMoTBue4/exec";

    const json = new XMLHttpRequest();
    json.onreadystatechange = function() {
        if ((json.readyState === 4) && (json.status === 200)) {
            const data = JSON.parse(json.responseText);

            for (let ii = 0; ii < 3; ++ii) {
                const description = data[ii].description;

                const regObj = new RegExp(/\.jpeg|\.jpg|\.png|\.gif/ig);
                let image_url = '';

                const array_size = description.match(regObj).length;
                for (let jj = 0; jj < array_size; ++jj) {
                    const result = regObj.exec(description);
                    if ( result ) {
                        // イメージファイルの直下で定義しているwidthのサイズを取ってくる
                        const w_point_start = description.indexOf('width="', result.index);

                        // 画像とwidthの位置が離れすぎている場合は画像とwidthが一致してないと考えて飛ばす
                        if ( w_point_start - result.index > 50 ) continue;

                        const w_point_end = description.indexOf('"', w_point_start+7);
                        const width_str = description.slice(w_point_start+7, w_point_end);
                        const width = Number.isNaN(Number(width_str)) ? 0 : Number(width_str);

                        if ( width < 150 ) continue;

                        const startpoint = description.lastIndexOf('https:', result.index);

                        if ( startpoint !== -1 ) {
                            const num_extension = result[0] === ".jpeg" ? 5 : 4;
                            image_url = description.slice(startpoint, result.index + num_extension);

                            if ( image_url ) break;
                        }
                    }
                }
                if ( !image_url ) {
                    const local = window.location;
                    image_url = local.origin + local.pathname + '/wp-content/themes/lipps-main-theme/images/P+LIPPS_logo.jpg';
                }

                let dateStr = new Date(data[ii].pubDate);
                const month = ('0' + (dateStr.getMonth() + 1)).slice(-2);
                const day = ('0' + (dateStr.getDate())).slice(-2);

                dateStr = dateStr.getFullYear() + '.' + month + '.' + day;
                let html = '<div class="blog-list">';
                html += '<div class="blog-thumbnail">';
                html += '<a href="' + data[ii].link + '" target="_blank"></a>';
                html += '<img src="' + image_url + '" />';
                html += '<p class="blog-title">' + data[ii].title + '</p>';
                html += '<p class="blog-date">' + dateStr + '</p>';
                html += '</div></div>';

                $('#lipps-blog-lists').append(html);
            }
        }
    }
    json.open("GET",url, true);
    json.send(null);
})();