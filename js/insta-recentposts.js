
(function() {
    const url = 'https://script.google.com/macros/s/AKfycbyOOFXAnu0sdRqkk6eWTPN470v8W5GgBTZddw_8/exec';

    const json = new XMLHttpRequest();
    json.onreadystatechange = function() {
        if ((json.readyState === 4) && (json.status === 200)) {
            const data = JSON.parse(json.responseText);

            for (let ii = 0; ii < 2; ++ii) {
                const thumbnail = data[ii].thumbnail;
                const permalink = data[ii].permalink;
                let html = '<div class="insta-item">';
                html += '<a href="'+permalink+'" target="_blank">';
                html += '<img src="'+thumbnail+'">';
                html += '</div>';

                $('#lipps-insta-recentposts').append(html);
            }
        }
    }
    json.open("GET",url, true);
    json.send(null);
})();


