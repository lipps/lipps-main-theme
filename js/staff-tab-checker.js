
(function() {
    const hash = window.location.hash;
    if ( !hash ) return;

    document.getElementById('staff-salon-'+hash.slice(1)).checked = true;
})();