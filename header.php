<?php
/**
 * The header for LIPPS theme
 *
 * @package LIPPS
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> >

<html>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">

  <!--[if lt IE 9]>
  <script src="<?php echo esc_url( get_theme_file_uri( '/js/html5shiv.min.js' ) ); ?>"></script>
  <![endif]-->

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header id="top" data-collapse="small" data-animation="left" data-duration="400" data-no-scroll="1" class="navbar w-nav">
  <div class="w-container header-container">
    <div class="nav-wrapper">
      <div class="brand">
        <div class="brand-logo-sub">
          <a href="<?php echo esc_url( home_url() )?>" alt="" class="brand-logo-sub-link"></a>
        </div>
        <div class="brand-logo">
	        <?php get_template_part('template-parts/header', 'logo')?>
        </div>
        <div class="brand-aside">
          <ul class="in-header-list">
            <li><a href="<?php echo esc_url(home_url('contact') ); ?>" class="button in-header-link primary">24H Web予約</a></li>
            <li><a href="<?php echo esc_url(home_url('coupon') ); ?>" class="button in-header-link">クーポン</a></li>
          </ul>
          <div class="header-sns">
            <ul class="sns-lists">
		        <?php
		        $locations = get_nav_menu_locations();
		        $menu = wp_get_nav_menu_object( $locations['menu-media'] );
		        $items = wp_get_nav_menu_items( $menu->term_id );
		        foreach ( $items as $item ) : ?>
              <li><a href="<?php echo esc_url( $item->url ); ?>" target="_blank" class="w-inline-block link-icon <?php echo $item->post_name; ?>"></a></li>
		        <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
      <?php get_template_part('template-parts/navigation', 'main')?>
    </div>
  </div>
  <?php
  global $template;
  $template_name = basename($template, '.php');
  //echo $template_name;
  ?>
  <div class="headerborder-line"></div>
</header>



