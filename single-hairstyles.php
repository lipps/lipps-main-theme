<?php
/**
 * The template for displaying pages for hairstyles
 *
 * @package LIPPS
 */
?>

<?php get_header(); ?>
<main>
	<?php while( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('lipps-container' ); ?>>
      <?php
      $post_id = get_the_ID();
      $type = get_the_terms($post_id, 'hairstyles_type');

			$custom_field = get_post_meta($post_id);

			$comment = '';
			if ( !empty( $custom_field['stylist_comment'] ) ) {
				$comment = $custom_field['stylist_comment'][0];
			}

			$products = '';
			if ( !empty( $custom_field['styling_items'] ) ) {
				$products = unserialize( $custom_field['styling_items'][0] );
			}

			$main_large = wp_get_attachment_image_src( $custom_field['photo_main'][0], 'large', false );
			$main_small = wp_get_attachment_image_src( $custom_field['photo_main'][0], 'medium', false );

			$front_large = wp_get_attachment_image_src( $custom_field['photo_front'][0], 'large', false );
			$front_small = wp_get_attachment_image_src( $custom_field['photo_front'][0], 'medium', false );

			$side_large = wp_get_attachment_image_src( $custom_field['photo_side'][0], 'large', false );
			$side_small = wp_get_attachment_image_src( $custom_field['photo_side'][0], 'medium', false );

			$back_large = wp_get_attachment_image_src( $custom_field['photo_back'][0], 'large', false );
			$back_small = wp_get_attachment_image_src( $custom_field['photo_back'][0], 'medium', false );
			?>
      <h5 class="lipps-content-subtitle"><?php echo strtoupper( $type[0]->slug )?></h5>
      <h2 class="lipps-content-heading">HAIR STYLE</h2>

      <div class="hairstyle-main-container">
        <h4 class="hairstyle-title narrow"><?php the_title();?></h4>
        <div class="hairstyle-main-imagebox">
          <input id="hairstyle-main-<?php the_ID(); ?>" type="radio" name="hairstyle-detail-image" class="hairstyle-input-main" checked>
          <input id="hairstyle-front-<?php the_ID(); ?>" type="radio" name="hairstyle-detail-image" class="hairstyle-input-front">
          <input id="hairstyle-side-<?php the_ID(); ?>" type="radio" name="hairstyle-detail-image" class="hairstyle-input-side">
          <input id="hairstyle-back-<?php the_ID(); ?>" type="radio" name="hairstyle-detail-image" class="hairstyle-input-back">
          <div class="hairstyle-main-image">
            <img src="<?php echo esc_url( $main_large[0] ); ?>" alt="">
          </div>
          <div class="hairstyle-front-image">
            <img src="<?php echo esc_url( $front_large[0] ); ?>" alt="">
          </div>
          <div class="hairstyle-side-image">
            <img src="<?php echo esc_url( $side_large[0] ); ?>" alt="">
          </div>
          <div class="hairstyle-back-image">
            <img src="<?php echo esc_url( $back_large[0] ); ?>" alt="">
          </div>

          <div class="hairstyle-min-imagebox">
            <label class="hairstyle-main-thumbnail" for="hairstyle-main-<?php the_ID(); ?>">
              <img src="<?php echo esc_url( $main_small[0] ); ?>" alt="" class="hairstyle-image">
            </label>
            <label class="hairstyle-front-thumbnail" for="hairstyle-front-<?php the_ID(); ?>">
              <img src="<?php echo esc_url( $front_small[0] ); ?>" alt="" class="hairstyle-image">
            </label>
            <label class="hairstyle-side-thumbnail" for="hairstyle-side-<?php the_ID(); ?>">
              <img src="<?php echo esc_url( $side_small[0] ); ?>" alt="" class="hairstyle-image">
            </label>
            <label class="hairstyle-back-thumbnail" for="hairstyle-back-<?php the_ID(); ?>">
              <img src="<?php echo esc_url( $back_small[0] ); ?>" alt="" class="hairstyle-image">
            </label>
          </div>
          <p class="model-name">Model: <?php echo $custom_field['model_name'][0]; ?></p>
        </div>
        <div class="hairstyle-text-container">
          <h4 class="hairstyle-title wide"><?php the_title();?></h4>
          <div class="hairstyle-content">
			      <?php echo $comment; ?>
          </div>
			    <?php echo do_blocks('<!-- wp:lipps/lipps-hairstyle-spec-block {"post_id":'.$post_id.'} /-->'); ?>

          <p>スタイリングアイテム:</p>
          <div class="styling-item-container">
			    <?php
			    foreach ( $products as $product ) :
				    $p = get_post($product);
            $product_acf = get_post_meta( $p->ID );
				    $product_url = $product_acf['product_url'][0];

				    $product_image = '';
				    if ( empty($product_acf['thumbnail']) ) {
					    $product_image = get_template_directory_uri() . '/images/P+LIPPS_logo.jpg';
				    }else {
					    $product_image = wp_get_attachment_image_src( $product_acf['thumbnail'][0], 'medium', false )[0];
				    } ?>
            <div class="styling-item">
              <a href="<?php echo $product_url; ?>" class="lipps-box-link"></a>
              <img src="<?php echo esc_url($product_image); ?>" alt="" class="product-image">
              <p class="styling-item-title"><?php echo $p->post_title; ?></p>
            </div>
			    <?php
			    endforeach;

			    $stylist = array('id' => '', 'name' => '', 'image' => '', 'salon' => '', 'salon_link' => '', 'phone_number' => '', 'reservation_url' => '', 'link' => '');
			    if ( !empty($custom_field['stylist']) ) {
            $stylist['id'] = $custom_field['stylist'][0];
				    $stylist_post = get_post($stylist['id']);
				    $stylist['name'] = $stylist_post->post_title;
				    $stylist['link'] = get_the_permalink($stylist_post->ID);
				    $stylist_acf = get_post_meta($stylist['id']);
				    $image = wp_get_attachment_image_src( $stylist_acf['photo_main'][0], 'medium', false );
				    $stylist['image'] = $image[0];

				    $salon_id = unserialize($stylist_acf['salon'][0])[0];
				    $salon_acf = get_post_meta($salon_id);
				    $stylist['salon'] = get_the_title($salon_id);

				    $stylist['salon_link'] = get_the_permalink( $salon_id );
				    $stylist['phone_number'] = $salon_acf['phone_number'][0];
				    $stylist['reservation_url'] = $stylist_acf['reservation_url'][0];
			    } ?>
          </div>
          <p>スタイリスト:</p>
          <div class="hairstyle-stylist-container">
            <div class="hairstyle-stylist-image-container">
              <div class="hairstyle-image-link">
                <a href="<?php echo $stylist['link']; ?>" class="lipps-box-link"></a>
                <img src="<?php echo esc_url( $stylist['image'] ); ?>" alt="" class="hairstyle-stylist-image">
              </div>
              <div class="hairstyle-stylist-text-container">
                <a href="<?php echo $stylist['link']; ?>" class="lipps-text-link hairstyle-stylist" ><?php echo $stylist['name']; ?></a>
                <a href="<?php echo $stylist['salon_link']; ?>" class="lipps-text-link hairstyle-stylist" ><?php echo $stylist['salon']; ?></a>
              </div>
            </div>
            <div class="reservation-container">
              <div class="reservation-item reservation">
                <a href="<?php echo esc_url($stylist['reservation_url']); ?>" class="lipps-box-link"></a>
                <p class="reservation-sentence">このスタイリストのWeb予約</p>
              </div>
              <div class="reservation-item phone-call">
                <a href="tel:<?php echo str_replace('-', '', $stylist['phone_number']); ?>" class="lipps-box-link"></a>
                <p class="reservation-sentence"><?php echo $stylist['phone_number']; ?> 電話予約</p>
              </div>
              <div class="reservation-item coupon">
                <a href="<?php echo esc_url(home_url('coupon')); ?>" class="lipps-box-link"></a>
                <p class="reservation-sentence">クーポンはこちら</p>
              </div>
            </div>
          </div>
        </div>
      </div>
			<?php
      if ( !empty( $stylist['name'] ) ) : ?>
        <div class="recent-hairstyle-title">スタイリスト <?php echo $stylist['name']; ?>の新着</div>
        <div class="recent-hairstyle-subtitle">HAIR STYLE</div>
			  <?php echo do_shortcode('[recent-hairstyle-list '.$stylist['id'] . ' ' . $post_id.']'); ?>
			<?php endif; ?>
    </article>
	<?php endwhile; ?>
</main>

<?php get_footer(); ?>

<?php
//          print_r($t[0]);
//          print_r(WP_Block_Type_Registry::get_instance()->get_registered( $t[0]['blockName'] )->render($t[0]['attrs'], '') );
//          print_r(render_block($t[0]));
//          $html = apply_filters( 'the_content', $html );
//          $html = str_replace( ']]>', ']]&gt;', $html );

?>