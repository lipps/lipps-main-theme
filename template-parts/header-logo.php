<?php

/**
 * For displaying company's logo on header
 *
 * @package LIPPS
 */

$custom_logo_id = get_theme_mod( 'custom_logo' );
$custom_logo_url = wp_get_attachment_image_url( $custom_logo_id, 'full');
if ( $custom_logo_url ===  false ) {
  $custom_logo_url = get_template_directory_uri() . "/images/lipps-logo.png";
}

echo '<a href="' . esc_url( home_url() ) . '"><img src="' . esc_url( $custom_logo_url ) . '" alt="" class="brand-image" ></a>';

?>