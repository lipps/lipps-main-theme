<?php
/**
 * For displaying media information on footer
 *
 * @package LIPPS
 */
?>

<div class="footer-group">
  <ul class="list w-list-unstyled">
    <?php
    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object( $locations['menu-media'] );
    $items = wp_get_nav_menu_items( $menu->term_id );

    foreach ( $items as $item ) : ?>
      <li class="list-item">
        <a href="<?php echo esc_url( $item->url ); ?>" target="_blank" class="list-item-link w-inline-block">
          <div class="nav-icon <?php echo $item->post_name; ?>"></div>
          <div class="text-block"><?php echo $item->title; ?></div>
        </a>
      </li>
    <?php endforeach; ?>
  </ul>
</div>










