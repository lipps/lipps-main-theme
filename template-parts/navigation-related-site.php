<?php
/**
 * For displaying related site on footer
 *
 * @package LIPPS
 */
?>

<?php
$locations = get_nav_menu_locations();
$menu = wp_get_nav_menu_object( $locations['menu-related-site'] );
$items = wp_get_nav_menu_items( $menu->term_id );

if ( !empty($items) ) : ?>
  <div class="footer-group">
    <ul class="list w-list-unstyled">
    <?php foreach ( $items as $item ) : ?>
      <li class="list-item">
        <a href="<?php echo esc_url( $item->url ); ?>" target="_black" class="list-item-link"><?php echo $item->title; ?></a>
      </li>
    <?php endforeach; ?>
  </ul>
</div>
<?php endif; ?>