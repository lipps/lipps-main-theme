<?php
/**
* For search hairstyles
*
* @package LIPPS
*/

wp_enqueue_script('amp', '//cdn.ampproject.org/v0.js');
wp_enqueue_script('amp-bind', '//cdn.ampproject.org/v0/amp-bind-0.1.js');
$TAB_BOX_CSS_CLASS = 'tab-box detail search-hairstyles';
?>

<div class="<?php echo $TAB_BOX_CSS_CLASS; ?>" [class]="q ? '<?php echo $TAB_BOX_CSS_CLASS; ?> inputed' : '<?php echo $TAB_BOX_CSS_CLASS; ?>'">
  <amp-state id="q">
    <script type="application/json">
    ""
    </script>
  </amp-state>
  <form method="get" action="https://www.google.com/search" target="_blank">
    <input value="" placeholder="検索" type="text" on="input-throttled:AMP.setState({q: event.value})">
    <input name="q" [value]="'site:lipps.co.jp ' + q" type="hidden">
    <input name="tbm" value="isch" type="hidden">
    <input type="submit" value="">
  </form>
</div>