<?php
/**
 * For displaying company's information on footer
 *
 * @package LIPPS
 */
?>

<div class="footer-group last">
  <ul class="list aside-link-list w-list-unstyled">
    <?php
    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object( $locations['menu-info'] );
    $items = wp_get_nav_menu_items( $menu->term_id );

    foreach ( $items as $item ) : ?>
      <li class="list-item list-item-wide">
        <a href="<?php echo esc_url( $item->url ); ?>" class="list-item-link"><?php echo $item->title; ?></a>
      </li>
    <?php endforeach; ?>
  </ul>
</div>










