<?php

/**
 * Archive template for hairstyle
 *
 * @package LIPPS
 */

?>

<?php get_header(); ?>
<main>
  <div class="tab-container">
    <div class="tab-box">
		<?php
    $hairstyle_link = get_permalink( get_page_by_path('hairstyle')->ID );  ?>

      <a href="<?php echo esc_url( $hairstyle_link . 'mens/' ); ?>" class="tab-item tab-top-text">MENS</a>
      <a href="<?php echo esc_url( $hairstyle_link . 'ladies/' ); ?>" class="tab-item tab-top-text selected">LADIES</a>
    </div>
    <div class="tab-box detail">
	    <?php
      $term_object = get_queried_object();
	    $category_terms = get_terms('hairstyles_ladys', array( 'order' => 'ASC', 'orderby' => 'ID' ));

	    foreach ( $category_terms as $cat_term ) :
          $tab_text_selected = ($cat_term->slug === $term_object->slug) ? 'selected' : ''; ?>

          <a href="<?php echo esc_url( $hairstyle_link . 'ladies/'.$cat_term->slug .'/#lipps-new-hairstyle' ); ?>" class="tab-item detail"><span class="tab-text <?php echo $tab_text_selected; ?>"><?php echo $cat_term->name; ?></span></a>
	    <?php endforeach; ?>
    </div>
    <?php get_template_part('template-parts/search-hairstyles'); ?>
  </div>
  <div class="lipps-container">
    <h2 class="lipps-content-heading">RANKING</h2>
    <div class="hairstyle-ranking-box">
		<?php
		$page_id = get_page_by_path('hairstyle' )->ID;
		$ranking_list = get_post_meta($page_id);

		for ( $ii = 1; $ii < 6; ++$ii ) :
			$post_id = unserialize($ranking_list['Ladys_'.$ii][0])[0];
			$custom_field = get_post_meta( $post_id );
			$image = wp_get_attachment_image_src( $custom_field['photo_main'][0], 'medium', false );

			$stylist_id = $custom_field['stylist'][0];
			$stylist_name = get_post($stylist_id)->post_title;

			$order_class = 'order-'. $ii;
			$ranking_mark = 'ranking-'. $ii;
			$ranking_image = 'ranking-image-'. $ii;
			?>
          <div class="lipps-image-item <?php echo $order_class; ?>">
            <a href="<?php echo get_the_permalink( $post_id ); ?>" target='_parent' class="lipps-box-link"></a>
            <img src="<?php echo esc_url( $image[0] ); ?>" alt="" class="<?php echo $ranking_image; ?>">
            <div class="hairstyle-ranking-container"><span class="hairstyle-ranking-common <?php echo $ranking_mark; ?>"></span><span class="ranking-order"></span><p class="hairstyle-ranking-number"><?php echo $ii ?></p></div>
            <p class="hairstyle-name"><?php echo get_post($post_id)->post_title; ?></p>
            <p class="hairstyle-stylist"><?php echo $stylist_name; ?></p>
          </div>
		<?php endfor; ?>
    </div>
  </div>
  <?php
  $hairstyle_category_title = strtoupper( $term_object->slug );
  $hairstyle_category_title = str_replace( '_', ' ', $hairstyle_category_title );
  ?>

  <div class="lipps-container">
    <h2 class="lipps-content-heading" id="lipps-new-hairstyle" ><?php echo $hairstyle_category_title; ?></h2>
    <div class="lipps-archive-box">
		<?php

		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1);

		// ログイン状態の管理者にも非公開ページはプレビューで見えないようにする
		$args = array(
			'post_status'    => 'publish',
			'post_type'      => 'hairstyles',
			'posts_per_page' => -1,
			'tax_query'      => array(
				array (
					'taxonomy' => 'hairstyles_ladys',
					'field'    => 'slug',
					'terms'    => array($term_object->slug) )
			)
		);

    $filtered = lipps_hairstyle_filter( $args, $paged, get_option( 'posts_per_page' ));
		foreach ( $filtered['posts'] as $item ) : ?>
      <div class="lipps-image-item hairstyle-archive">
        <a href="<?php echo $item['link'] ?>"  class="lipps-box-link"></a>
        <img src="<?php echo esc_url( $item['image'] ); ?>" alt="" class="hairstyle-image">
        <p class="hairstyle-name"><?php echo $item['hairstyle_name']; ?></p>
        <p class="hairstyle-stylist"><?php echo $item['stylist_name']; ?></p>
      </div>
		<?php endforeach;
		// ページャーの表示
		if ( function_exists( 'lipps_pagination' ) ) {
			lipps_pagination( $filtered['max_num_pages'], $paged, 5, '#lipps-new-hairstyle');
		}

		?>
    </div>
  </div>
</main>
<?php get_footer(); ?>

<?php
function lipps_hairstyle_filter( $args, $paged, $posts_per_page ) {

  $page_counter = $item_counter = 0;
  $filtered_posts = array( array() );
  $posts = get_posts( $args );
  foreach ( $posts as $hairstyle ) {
    if ( $item_counter == $posts_per_page ) {
      $page_counter++;
      $item_counter = 0;
    }
    $custom_field = get_post_meta( $hairstyle->ID );

    if ( empty( $custom_field['stylist']) ) continue;
    $stylist_id = $custom_field['stylist'];
    if ( empty( $stylist_id ) or $stylist_id[0] === '' ) continue;

    $stylist_post = get_post( $stylist_id[0] );
    if ( empty( $stylist_post ) or $stylist_post->post_status === 'private' ) continue;

    if ( empty( $custom_field['photo_main'] ) or empty( $custom_field['photo_main'][0] ) ) continue;
    $image = wp_get_attachment_image_src( $custom_field['photo_main'][0], 'medium', false );

	  $tmp = array(
        $hairstyle->ID => array(
            'link' => get_the_permalink( $hairstyle->ID ),
            'image' => $image[0],
            'hairstyle_name' => $hairstyle->post_title,
            'stylist_name' => $stylist_post->post_title
        )
    );

	  $filtered_posts[$page_counter] = $item_counter === 0 ? $tmp : $filtered_posts[$page_counter] + $tmp;
    $item_counter++;
  }
  $page_max = $page_counter + 1;
  $array_num_of_paged = $paged - 1;
  if ( $paged > $page_max ) { $array_num_of_paged = $page_conter; }

  return array('posts' => $filtered_posts[$array_num_of_paged], 'max_num_pages' => $page_max );
}
?>


