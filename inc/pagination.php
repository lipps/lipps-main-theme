<?php

function lipps_pagination( $pages, $paged, $max_num_display = 5, $anchor='' ) {
	$max_num_pages = (int) $pages;

	$text_first = "<<&emsp;Latest&emsp;";
	$text_last  = "Last&emsp;>>";
	$ellipsis   = "...&emsp;";

	$width = (int) floor( $max_num_display / 2 );

	echo '<div class="lipps-pagination">';

	if ( $max_num_pages > 1 ) {
		// 2ページ以上の時
		if ( $max_num_pages < $max_num_display + 1 ) {
			for ( $ii = 1; $ii < $max_num_pages + 1; ++$ii ) {
				if ( $paged === $ii ) {
					echo '<span class="current pager">', $ii . '&emsp;', '</span>';
				} else {
					echo '<a href="', get_pagenum_link( $ii ).$anchor, '" class="lipps-pagination-link">', $ii . '&emsp;', '</a>';
				}
			}
		} elseif ( $paged <= $width + 1 ) {
			// widthよりも小さいページにいる場合
			for ( $ii = 1; $ii < $max_num_display + 1; ++$ii ) {
				if ( $paged === $ii ) {
					echo '<span class="current pager">', $ii . '&emsp;', '</span>';
				} else {
					echo '<a href="', get_pagenum_link( $ii ).$anchor, '" class="lipps-pagination-link">', $ii . '&emsp;', '</a>';
				}
			}
			echo '<span class="lipps-pagination-link">', $ellipsis, '</span>';
			echo '<a href="', get_pagenum_link( $max_num_pages ).$anchor, '" class="lipps-pagination-link">', $text_last, '</a>';

		} elseif ( $paged >= ( $max_num_pages - $width ) ) {
			// (全ページ - width)よりも大きいページにいる場合
			echo '<a href="', get_pagenum_link( 1 ), '" class="lipps-pagination-link">', $text_first, '</a>';
			echo '<span class="lipps-pagination-link">', $ellipsis, '</span>';

			for ( $ii = ( $max_num_pages - $max_num_display ) + 1; $ii < $max_num_pages + 1; ++$ii ) {
				if ( $paged === $ii ) {
					echo '<span class="current pager">', $ii . '&emsp;', '</span>';
				} else {
					echo '<a href="', get_pagenum_link( $ii ).$anchor, '" class="lipps-pagination-link">', $ii . '&emsp;', '</a>';
				}
			}

		} else {
			// 上記以外ならば現在のページの前後 width 分を表示させる
			echo '<a href="', get_pagenum_link( 1 ).$anchor, '" class="lipps-pagination-link">', $text_first, '</a>';
			echo '<span class="lipps-pagination-link">', $ellipsis, '</span>';
			for ( $ii = ( $paged - $width ); $ii < ( $paged + $width ) + 1; ++$ii ) {
				//echo 'paged = ', $paged, ' ii = ', $ii;
				if ( $paged === $ii ) {
					echo '<span class="current pager">', $ii . '&emsp;', '</span>';
				} else {
					echo '<a href="', get_pagenum_link( $ii ).$anchor, '" class="lipps-pagination-link">', $ii . '&emsp;', '</a>';
				}
			}
			echo '<span class="lipps-pagination-link">', $ellipsis, '</span>';
			echo '<a href="', get_pagenum_link( $max_num_pages ).$anchor, '" class="lipps-pagination-link">', $text_last, '</a>';
		}
	}
	echo '</div>';
}
