<?php
/**
 * Short code collection
 *
 * @package LIPPS
 */

function lipps_hairstyle_gallery() {
	$page_id = get_page_by_path( 'hairstyle' )->ID;
	$ranking_list = get_post_meta( $page_id );

	$html = array( 'Ladies' => array('',''), 'Mens' => array('',''));
	foreach ( array('Ladies', 'Mens') as $ch ) {
		for ( $ii = 1; $ii < 3; ++$ii ) {
			$post_id = unserialize( $ranking_list[$ch.'_'.$ii][0])[0];
			$custom_field = get_post_meta( $post_id );
			$image = wp_get_attachment_image_src ( $custom_field['photo_main'][0], 'medium', false );

			$stylist_id = $custom_field['stylist'][0];
			$stylist_name = get_post( $stylist_id )->post_title;

			$html[$ch][$ii-1] = '<div class="lipps-image-item hairstyle-gallery">';
			$html[$ch][$ii-1] .= '<a href="'.get_the_permalink( $post_id ).'" class="lipps-box-link"></a>';
			$html[$ch][$ii-1] .= '<img src="'.esc_url( $image[0] ).'" alt="" class="hairstyle-image">';
			$html[$ch][$ii-1] .= '<p class="hairstyle-name">'.get_post($post_id)->post_title.'</p>';
			$html[$ch][$ii-1] .= '<p class="hairstyle-stylist">'.$stylist_name.'</p>';
			$html[$ch][$ii-1] .= '</div>';
		}
	}

	return sprintf('
		<div class="lipps-container-narrow">
		  <div class="hairstyle-gallery-box">
            <div class="hairstyle-gallery-no1">
            <div class="hairstyle-ranking-container"><span class="hairstyle-ranking-common ranking-1"></span><span class="ranking-order"></span><p class="hairstyle-ranking-number">1</p></div>
              <div class="hairstyle-gallery-items">
                %1$s %2$s
              </div> 		  
            </div>
            <div class="hairstyle-gallery-no2">
              <div class="hairstyle-ranking-container"><span class="hairstyle-ranking-common ranking-2"></span><span class="ranking-order"></span><p class="hairstyle-ranking-number">2</p></div>
              <div class="hairstyle-gallery-items">              
                %3$s %4$s
              </div>
            </div>             		  
		  </div>
		</div>', $html['Mens'][0], $html['Ladies'][0], $html['Mens'][1], $html['Ladies'][1]
	);
}
add_shortcode('hairstyle-gallery', 'lipps_hairstyle_gallery');


function lipps_twitter_timeline($args) {
	$handle = ltrim($args[0], '@');
	return sprintf('
    <div class="sns-box">
      <div class="sns-title">
        <div class="nav-icon twitter timeline"></div>
        <div class="sns-sitename">TWITTER</div>
      </div> 
      <div class="twitter-widget"> 
	    <a class="twitter-timeline" data-height="350" data-lang="en" data-chrome="nofooter" href="https://twitter.com/%1$s?ref_src=twsrc%5Etfw">Tweets by %1$s</a> 
	    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
	  </div>
	</div>
   ', $handle
	);
}
add_shortcode( 'twitter-timeline', 'lipps_twitter_timeline');


function lipps_youtube_recentposts() {
	$script_dir = get_template_directory_uri() . '/js/youtube-viewer.js';

	return sprintf('
    <div class="sns-box">
      <div class="sns-title">
        <div class="nav-icon youtube timeline"></div>
        <div class="sns-sitename">YOUTUBE</div>
      </div> 
      <div id="lipps-youtube-recentposts" class="youtube-container">      
        <script async src="%1$s" charset="utf-8"></script> 
	  </div>
	</div>
   ', $script_dir
	);
}
add_shortcode( 'youtube-recentposts', 'lipps_youtube_recentposts');


function lipps_instagram_recentposts() {
	$script_dir = get_template_directory_uri() . '/js/insta-recentposts.js';
	return sprintf('
    <div class="sns-box">
      <div class="sns-title">
        <div class="nav-icon instagram timeline"></div>
        <div class="sns-sitename">INSTAGRAM</div>
      </div>
      <div id="lipps-insta-recentposts" class="insta-container">      
        <script async src="%1$s" charset="utf-8"></script> 
	  </div>
	</div>
   ', $script_dir
	);
}
add_shortcode( 'instagram-recentposts', 'lipps_instagram_recentposts');


function lipps_blog_recentposts() {
	$script_dir = get_template_directory_uri() . '/js/blog-recentposts.js';

	return sprintf('
    <div class="blog-list-container">
      <div id="lipps-blog-lists" class="blog-lists">
      <script async src="%1$s" charset="utf-8"></script> 
      </div> 
	</div>
   ', $script_dir
	);
}
add_shortcode( 'blog-recentposts', 'lipps_blog_recentposts');


function lipps_recent_news_posts() {
	$posts = get_posts( array(
			'post_type' => 'news',
			'post_status' => 'publish',
			'posts_per_page' => 4
		)
	);
	$html = '';
	foreach ( $posts as $news ) {
		$image_url = get_the_post_thumbnail_url( $news->ID, 'medium' );
		if ( ! $image_url ) {
			$image_url = get_template_directory_uri() . '/images/P+LIPPS_logo.jpg';
		}

		$html .= '<div class="lipps-image-item news-headline recent-posts">';
		$html .= '<a href="'.get_the_permalink( $news->ID ).'" class="lipps-box-link"></a>';
		$html .= '<img src="'.esc_url( $image_url ).'" alt="" class="news-image">';
		$html .= '<p class="news-headline title">'.$news->post_title.'</p>';
		$html .= '<p class="news-headline date">'.get_the_time( 'Y.m.d', $news->ID ).'</p>';
		$html .= '</div>';
	}

	return sprintf('
		<div class="news-container alignfull">
		  <div class="news-articles recent-post">
		    %1$s
		  </div>
          <div class="news-return-link-container">
            <a href="'. get_post_type_archive_link( 'news'  ).'" class="news-return-link right">MORE ></a>
          </div>
		</div>', $html
	);
}
add_shortcode('recent-news-posts', 'lipps_recent_news_posts');


function lipps_salon_list() {
	$salons = get_posts( array(
        'post_status' => 'publish',
        'post_type' => 'salon',
        'orderby' => 'date',
        'order' => 'ASC',
        'posts_per_page' => -1
    ));
	$html = '';
    foreach( $salons as $salon ) {
      $custom_field = get_post_meta( $salon->ID );
      $roman_name = $custom_field['romanization'][0];
      $thumbnail_url = get_the_post_thumbnail_url( $salon->ID, 'medium' );

      $html .= '<div class="lipps-image-item salon-archive">';
      $html .= '<a href="'. get_the_permalink( $salon->ID ) . '" class="salon-link">';
      $html .= '<img src="'.esc_url( $thumbnail_url ).'" alt="" class="salon-image">';
      $html .= '<p class="salon-headline name">'.$salon->post_title.'</p>';
      $html .= '<div class="salon-black-filter"></div>';
      $html .= '</a></div>';
    }

    return sprintf('
      <div class="lipps-archive-box salon">
      %1$s
      </div>
    ', $html
    );
}
add_shortcode('salon-list', 'lipps_salon_list');


function lipps_recent_hairstyle_list( $args ) {
	$post_id = -1;

	if ( !empty($args[1]) ) {
		$post_id = $args[1];
	}
	$html = '';

	$stylist_id = $args[0];
	$stylist_post = get_post($stylist_id);

	if ( empty( $stylist_post ) or $stylist_post->post_status === 'private' ) return $html;

	$first = ''; $second = '';
	$counter = 0;

	$stylist_IDs = lipps_get_same_post_title_id($stylist_id, 'staff');

	$post_list = get_posts( array(
		  'post_status' => 'publish',
		  'post_type' => 'hairstyles',
		  'posts_per_page' => -1,
		  'meta_key' => 'stylist',
		  'orderby' => 'date',
		  'order' => 'DESC',
		  'meta_value' => $stylist_IDs,)
	);

	foreach ( $post_list as $item ) {
	  if ( $item->ID === (int)$post_id ) continue;

	  $hairstyle_acf = get_post_meta( $item->ID );
	  $image = wp_get_attachment_image_src( $hairstyle_acf['photo_main'][0], 'medium', false );
	  $tmp = '';
	  $tmp .= '<div class="lipps-image-item recent-hairstyle-archive">';
	  $tmp .= '<a href="' . get_the_permalink( $item->ID ) . '"  class="lipps-box-link"></a>';
	  $tmp .= '<img src="' . esc_url( $image[0] ) . '" alt="" class="recent-hairstyle-image">';
	  $tmp .= '<p class="hairstyle-name">' . $item->post_title . '</p>';
	  $tmp .= '</div>';

	  if ( $counter < 6 ) { $first .= $tmp; }
	  else { $second .= $tmp; }
	  $counter += 1;
	}

	return sprintf('
      <div class="recent-hairstyle-container">
        <div class="nearest-hairstyle">
          %1$s
        </div>  
        <input id="recent-hairstyle-%2$s" type="checkbox" name="recent-hairstyle-switch" class="recent-hairstyle-input">
        <label class="recent-hairstyle-item-open" for="recent-hairstyle-%2$s">%3$sのヘアスタイルをすべてみる<span class="arrow-down"></span></label>
        <div id="recent-hairstyle-%2$s" class="recent-hairstyle-box">
          %4$s
        </div>
        <label class="recent-hairstyle-item-close" for="recent-hairstyle-%2$s">一部隠す<span class="arrow-up"></span></label>                
      </div>
    ',$first, $post_id, $stylist_post->post_title, $second
	);
}
add_shortcode('recent-hairstyle-list', 'lipps_recent_hairstyle_list');


function lipps_salon_hairstyle_list() {
	$post_id = get_the_ID();
	if ( ! $post_id ) return '';

	$staff_posts = get_posts( array(
			'post_status' => 'publish',
			'post_type' => 'staff',
			'posts_per_page' => -1,
			'meta_query' => array(
					array(
						'key' => 'salon',
						'value' => $post_id,
						'compare' => 'LIKE',
					),
			))
	);

	$staff_IDs = array();
	foreach ( $staff_posts as $item ) {
		$staff_IDs = array_merge($staff_IDs, lipps_get_same_post_title_id($item->ID, 'staff') );
	}

	$hairstyle_posts = get_posts( array(
			'post_status' => 'publish',
			'post_type' => 'hairstyles',
	  	'posts_per_page' => 9999,
			'meta_key' => 'stylist',
			'orderby' => 'date',
			'order' => 'DESC',
			'meta_value' => $staff_IDs,)
	);

	$first = ''; $second = '';
	$counter = 0;

	foreach ( $hairstyle_posts as $item ) {
		if ( $item->ID === (int)$post_id ) continue;

		$hairstyle_acf = get_post_meta( $item->ID );
		$stylist_name = get_post($hairstyle_acf['stylist'][0])->post_title;

		$image = wp_get_attachment_image_src( $hairstyle_acf['photo_main'][0], 'medium', false );
		$tmp = '';
		$tmp .= '<div class="lipps-image-item recent-hairstyle-archive">';
		$tmp .= '<a href="' . get_the_permalink( $item->ID ) . '"  class="lipps-box-link"></a>';
		$tmp .= '<img src="' . esc_url( $image[0] ) . '" alt="" class="recent-hairstyle-image">';
		$tmp .= '<p class="hairstyle-name">' . $item->post_title . '</p>';
		$tmp .= '<p class="hairstyle-stylist">' . $stylist_name . '</p>';

		$tmp .= '</div>';

		if ( $counter < 6 ) { $first .= $tmp; }
		else { $second .= $tmp; }
		$counter += 1;
	}

	return sprintf('
    <div class="recent-hairstyle-container">
      <div class="nearest-hairstyle">
        %1$s
      </div>  
      <input id="recent-hairstyle-%2$s" type="checkbox" name="recent-hairstyle-switch" class="recent-hairstyle-input">
      <label class="recent-hairstyle-item-open" for="recent-hairstyle-%2$s">%3$sのヘアスタイルをすべてみる<span class="arrow-down"></span></label>
      <div id="recent-hairstyle-%2$s" class="recent-hairstyle-box">
        %4$s
      </div>
      <label class="recent-hairstyle-item-close" for="recent-hairstyle-%2$s">一部隠す<span class="arrow-up"></span></label>                
    </div>
    ',$first, $post_id, get_the_title(), $second
	);
}
add_shortcode('salon-hairstyle-list', 'lipps_salon_hairstyle_list');