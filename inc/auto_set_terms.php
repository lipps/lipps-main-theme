<?php

function lipps_auto_set_terms( $post_id ) {

	if ( wp_is_post_revision( $post_id) ) { return; }

	if ( get_post_type( $post_id ) !== 'hairstyles' ) { return; }
	$custom_field = get_post_meta( $post_id );

	if ( empty($custom_field['category']) ) { return; }
	$category = $custom_field['category'][0];

	// 前に登録されているものが残っていると悪影響を及ぼすため、一旦クリアする
	wp_set_object_terms( $post_id, null, 'hairstyles_mens' );
	wp_set_object_terms( $post_id, null, 'hairstyles_ladys' );
	wp_set_object_terms( $post_id, null, 'hairstyles_type' );

	$type_term = '';
	$hairstyle_category = '';
	$category_term = '';

	switch( $category ) {
		case 1:
			$type_term = 'Ladies';
			$hairstyle_category = 'hairstyles_ladys';
			$category_term = 'ショート';
			break;
		case 2:
			$type_term = 'Ladies';
			$hairstyle_category = 'hairstyles_ladys';
			$category_term = 'ミディアム';
			break;
		case 3:
			$type_term = 'Ladies';
			$hairstyle_category = 'hairstyles_ladys';
			$category_term = 'ボブ';
			break;
		case 4:
			$type_term = 'Ladies';
			$hairstyle_category = 'hairstyles_ladys';
			$category_term = 'ロング';
			break;
		case 11:
			$type_term = 'Mens';
			$hairstyle_category = 'hairstyles_mens';
			$category_term = 'ベリーショート';
			break;
		case 12:
			$type_term = 'Mens';
			$hairstyle_category = 'hairstyles_mens';
			$category_term = 'ショート';
			break;
		case 13:
			$type_term = 'Mens';
			$hairstyle_category = 'hairstyles_mens';
			$category_term = 'ミディアム';
			break;
		case 14:
			$type_term = 'Mens';
			$hairstyle_category = 'hairstyles_mens';
			$category_term = 'ビジネス';
			break;
		case 21:
			$type_term = 'Mens';
			$hairstyle_category = 'hairstyles_mens';
			$category_term = 'オトナLIPPS';
			break;
		default:
			$type_term = '';
			$hairstyle_category = '';
			$category_term = '';
			break;
	}

	if ( $hairstyle_category !=='' && $category_term !=='' && $type_term !== '' ) {
		wp_set_object_terms( $post_id, $category_term, $hairstyle_category );
		wp_set_object_terms( $post_id, $type_term, 'hairstyles_type' );
	}
}
add_action('save_post', 'lipps_auto_set_terms');
