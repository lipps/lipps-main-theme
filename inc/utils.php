<?php

function lipps_get_same_post_title_id( $post_id, $post_type ) {
	$title = get_post($post_id)->post_title;

	$same_posts = get_posts( array(
			'post_status' => 'publish',
			'post_type' => $post_type,
			'posts_per_page' => -1,
			'title' => $title,)
	);

	$IDs = array();
	foreach ($same_posts as $p) {
		$IDs[] = $p->ID;
	}

	return $IDs;
}

