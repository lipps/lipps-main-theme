<?php

/**
 * Template Name: News archive
 *
 * @package LIPPS
 */
?>

<?php get_header(); ?>

  <main>
    <div class="lipps-container">
      <div class="lipps-content-heading-ruby">お知らせ</div>
      <h2 class="lipps-content-heading">NEWS</h2>
      <div class="news-container">
      <?php
      $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1);

      $args = array(
	        'post_status' => 'publish',
          'post_type'      => 'news',
          'posts_per_page' => get_option( 'posts_per_page' ),
          'paged'          => $paged
      );

      $local_query = new WP_Query( $args );

      // ページャーの表示
      if ( function_exists( 'lipps_pagination' ) ) {
          lipps_pagination( $local_query->max_num_pages, $paged );
      } ?>

        <div class="news-articles">
        <?php
        $posts = get_posts( $args );
        foreach ( $posts as $news ) :
	        $thumbnail_url = get_the_post_thumbnail_url( $news->ID, 'medium' );
            if ( ! $thumbnail_url ) {
              $thumbnail_url = get_template_directory_uri() . '/images/P+LIPPS_logo.jpg';
            } ?>

          <div class="lipps-image-item news-headline">
            <a href="<?php echo get_the_permalink( $news->ID ); ?>" class="lipps-box-link"></a>
            <img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="" class="news-image">
            <p class="news-headline title"><?php echo $news->post_title; ?></p>
            <p class="news-headline date"><?php echo get_the_time( 'Y.m.d', $news->ID ); ?></p>
          </div>
        <?php endforeach; ?>
        </div>
        <?php
        // ページャーの表示
        if ( function_exists( 'lipps_pagination' ) ) {
              lipps_pagination( $local_query->max_num_pages, $paged );
        }
        ?>
      </div>
    </div>
  </main>
<?php get_footer(); ?>
