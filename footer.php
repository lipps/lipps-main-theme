<?php
/**
 * The footer for LIPPS theme
 *
 * @package LIPPS
 */
?>

<footer class="footer">
  <div class="footer-alignment">
    <div class="footerborder-line"></div>
    <div class="footer-container w-container">
		<?php get_template_part( 'template-parts/navigation', 'media' ) ?>
		<?php get_template_part( 'template-parts/navigation', 'related-site' ) ?>
		<?php get_template_part( 'template-parts/navigation', 'info' ) ?>
      <div class="footer-logo-sub"></div>
    </div>

    <div class="w-container">
      <div class="footer-brand-wrapper">
		  <?php get_template_part( 'template-parts/footer', 'logo' ) ?>
        <div>&copy LIPPS</div><div class="footer-print-kana">リップス</div>
        <a href="#top" class="to-top-link w-inline-block">
          <img src="<?php echo esc_url( get_template_directory_uri() . "/images/up_arrow.png" ); ?>"
               alt="" class="to-top-link-arrow">
          <div>TOP</div>
        </a>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>

